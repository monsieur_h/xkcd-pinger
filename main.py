#!/bin/python
import argparse
from collections import deque
import datetime
import re
import subprocess
import time

import matplotlib.pyplot as plt


def clamp(value, mini, maxi):
    if value < mini:
        return mini

    if value > maxi:
        return maxi

    return value

def ping(host, count=1):
    ping_process = subprocess.Popen(["ping", "-c", str(count), host], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    out, error = ping_process.communicate()
#     print out
#     print error
    ms_time_string = re.search('time=([0-9\.]+)', out)

    if ms_time_string:
        return float(ms_time_string.group(1))
    else:
        return None

def loop(host, interval=1):
    must_ping = True
    previous_values = HistoryManager()
    plt.ion()
    plt.xkcd()
    plt.title("Pinging {}".format(host))
    plt.xlabel("Time")
    plt.ylabel("Latency")

    while must_ping:
        start = time.time()
        response_time = ping(host)

        previous_values.add(response_time)

        draw(previous_values)
        elapsed = time.time() - start
        time_to_sleep = interval - elapsed
        time_to_sleep = clamp(time_to_sleep, 0, interval)
#         time.sleep(time_to_sleep)
        plt.pause(time_to_sleep)


MAX_TEXT = None
def draw(history_instance):
    global MAX_TEXT
    latency, timestamp = history_instance.get()
    axis_scale = [min(timestamp), max(timestamp), min(latency), max(latency)]
    if len(timestamp) > 1:
        plt.axis(axis_scale)
        plt.plot(timestamp, latency, 'red')
        plt.draw()

        local_maxi = max(latency)
        index = latency.index(local_maxi)
        x = timestamp[index]
        y = latency[index]
        if MAX_TEXT:
            MAX_TEXT.remove()
        MAX_TEXT = plt.text(x, y, "Max : {}".format(local_maxi))


class HistoryManager(object):
    def __init__(self, max_size=50):
        self._queue = deque(maxlen=max_size)
        self._time = deque(maxlen=max_size)

    def add(self, value):
        self._queue.append(value)
        self._time.append(time.time())
        print self._queue

    def get(self):
        return list(self._queue), list(self._time)


def main():
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('-u', '--host', dest='host', required=False, help='The url to send the ping to', default='8.8.8.8')
    my_parser.add_argument('-i', '--interval', dest='interval', required=False, help='The interval between each ping command', default=1)
    my_config = my_parser.parse_args()

    print my_config
    loop(my_config.host, my_config.interval)


if __name__ == "__main__":
    main()
